﻿using FluentAssertions;

namespace SortArrayDESC.Tests
{
    public class SortArrayDescTests
    {
        [Fact]
        public void SortArrayDesc_ShouldReturnEmptyArray_WhenArrayParamIsEmpty()
        {
            // arrange
            var param = Array.Empty<int>();
            var expected = Array.Empty<int>();

            // act
            var actual = Program.SortArrayDesc(param);

            // assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void SortArrayDesc_ShouldReturnCorrectArray_WhenArrayParamIsNotEmpty()
        {
            // arrange
            var param = new int[] { 3 };
            var expected = new int[] { 3 };

            // act
            var actual = Program.SortArrayDesc(param);

            // assert
            actual.Should().BeEquivalentTo(expected);
        }


        [Fact]
        public void SortArrayDesc_ShouldReturnCorrect_WhenArrayParamHave2Item()
        {
            // arrange
            var param = new int[] { -1, 3 };
            var expected = new int[] { 3, -1 };

            // act
            var actual = Program.SortArrayDesc(param);

            // assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void SortArrayDesc_ShouldReturnCorrect_WhenArrayParamHave5Item()
        {
            // arrange
            var param = new int[] { 3, -1, -2, 5, 5 };
            var expected = new int[] { 5, 5, 3, -2, -1 };

            // act
            var actual = Program.SortArrayDesc(param);

            // assert
            actual.Should().BeEquivalentTo(expected);
        }
    }
}
